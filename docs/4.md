# MVP

模型矩阵（Model Matrix）、视图矩阵（View Matrix）、投影矩阵（Projection Matrix）

1km外有很多包裹

1. 每个包裹拿过来看
[
    1 0 -1000
    0 1 -1000
    0 0 1
]

2. 人走过去看包裹

[
    1 0 1000
    0 1 1000
    0 0 1
]

开销不一样，但都达到了目的，互为逆矩阵

矩阵，先绕 z 旋转 45度，再沿着 z 平移 100：

[
    
]

参考：
- OpenGL矩阵变换的数学推导 https://cloud.tencent.com/developer/article/1389550
- https://blog.csdn.net/qq_18229381/article/details/77941325
- http://www.songho.ca/opengl/gl_projectionmatrix.html
- https://www.cnblogs.com/pengkunfan/p/3790199.html
- https://learnopengl-cn.github.io/

