# canvas 学习

部分 demo 资料来自网络

- (太阳、月亮、地球，运转图)[./canvas-sml.html]
- (tooltip 鼠标移入、移出效果)[./canvas-event.html]
- (绘制立方体)[./canvas-cube.html]
- (click点击事件)[./canvas-click.html]
- (弹跳小球)[./canvas-ball.html]

计算机图形学：http://staff.ustc.edu.cn/~lgliu/Resources/CG/What_is_CG.htm
